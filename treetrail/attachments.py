from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from treetrail.api_v1 import (get_attachment_tree_root,
                              get_attachment_trail_root, get_attachment_poi_root)
from treetrail.plantekey import get_thumbnail_root, get_img_root, get_img_type_root

attachment_app = FastAPI()

attachment_app.mount("/plantekey/img", StaticFiles(directory=get_img_root()),
                     name="plantekey_img")
attachment_app.mount("/plantekey/thumb", StaticFiles(directory=get_thumbnail_root()),
                     name="plantekey_thumb")
attachment_app.mount("/plantekey/type", StaticFiles(directory=get_img_type_root()),
                     name="plantekey_type")

attachment_app.mount("/trail", StaticFiles(directory=get_attachment_trail_root()),
                     name="trail")
attachment_app.mount("/tree", StaticFiles(directory=get_attachment_tree_root()),
                     name="tree")
attachment_app.mount("/poi", StaticFiles(directory=get_attachment_poi_root()),
                     name="poi")