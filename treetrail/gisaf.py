from datetime import datetime
from typing import Annotated

from sqlalchemy import String
from geoalchemy2 import Geometry, WKBElement
from sqlmodel import Field

from treetrail.models import BaseModel

class GisafTree(BaseModel, table=True):
    __tablename__: str = "gisaf_tree" # type: ignore

    plantekey_id: int = Field(foreign_key='plantekey.id', primary_key=True)
    data: int
    create_date: datetime = Field(default_factory=datetime.now)
    geom: Annotated[str, WKBElement] = Field(
        sa_type=Geometry('POINTZ', srid=4326, dimension=3))

    photo: str = Field(sa_type=String(250)) # type: ignore
