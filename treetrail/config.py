from os import environ
from pathlib import Path
from typing import Any, Type, Tuple
from yaml import safe_load
import logging

from pydantic_settings import (
    BaseSettings,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
)
from pydantic.v1.utils import deep_update

from treetrail._version import __version__


logger = logging.getLogger(__name__)
ENV = environ.get("env", "prod")

config_files = [
    Path(Path.cwd().root) / "etc" / "treetrail" / ENV,
    Path.home() / ".local" / "treetrail" / ENV,
]


def config_file_settings() -> dict[str, Any]:
    config: dict[str, Any] = {}
    for p in config_files:
        for suffix in {".yaml", ".yml"}:
            path = p.with_suffix(suffix)
            if not path.is_file():
                logger.debug(f"No file found at `{path.resolve()}`")
                continue
            logger.info(f"Reading config file `{path.resolve()}`")
            if path.suffix in {".yaml", ".yml"}:
                config = deep_update(config, load_yaml(path))
            else:
                logger.info(f"Unknown config file extension `{path.suffix}`")
    return config


def load_yaml(path: Path) -> dict[str, Any]:
    with Path(path).open("r") as f:
        config = safe_load(f)
    if not isinstance(config, dict):
        raise TypeError(f"Config file has no top-level mapping: {path}")
    return config


def create_dirs():
    """
    Create the directories needed for a proper functioning of the app
    """
    ## Avoid circular imports
    from treetrail.api_v1 import attachment_types

    for type in attachment_types:
        base_dir = Path(conf.storage.root_attachment_path) / type
        base_dir.mkdir(parents=True, exist_ok=True)
    logger.info(f"Cache dir: {get_cache_dir()}")
    get_cache_dir().mkdir(parents=True, exist_ok=True)


def get_cache_dir() -> Path:
    return Path(conf.storage.root_cache_path)


class DB(BaseSettings):
    # uri: str
    host: str = "localhost"
    port: int = 5432
    user: str = "treetrail"
    db: str = "treetrail"
    password: str
    debug: bool = False
    info: bool = False
    pool_size: int = 10
    max_overflow: int = 10
    echo: bool = False

    def get_sqla_url(self):
        return f"postgresql+asyncpg://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"

    def get_pg_url(self):
        return f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"


class App(BaseSettings):
    title: str = "Tree Trail"


class Storage(BaseSettings):
    root_attachment_path: str = "/usr/local/treetrail/attachments"
    root_cache_path: str = "/usr/local/treetrail/cache"


class Tiles(BaseSettings):
    baseDir: str = "/path/to/mbtiles_files_dir"
    useRequestUrl: bool = True
    spriteBaseDir: str = "/path/to/mbtiles_sprites_dir"
    spriteUrl: str = "/tiles/sprite/sprite"
    spriteBaseUrl: str = "https://treetrail.example.org"
    osmBaseDir: str = "/path/to/osm"


class Map(BaseSettings):
    zoom: float = 14.0
    pitch: float = 0.0
    lat: float = 12.0000
    lng: float = 79.8106
    bearing: float = 0
    background: str = "OpenFreeMap"


class Geo(BaseSettings):
    simplify_geom_factor: int = 10000000
    simplify_preserve_topology: bool = False


class Security(BaseSettings):
    """
    JWT security configuration
    """

    secret_key: str = "0" * 64
    '''Generate with eg.: "openssl rand -hex 32"'''
    access_token_expire_minutes: float = 30


class ExternalMapStyle(BaseSettings):
    name: str
    url: str


class Config(BaseSettings):
    model_config = SettingsConfigDict(
        # env_prefix='treetrail_',
        env_nested_delimiter="__",
    )

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> Tuple[PydanticBaseSettingsSource, ...]:
        return (env_settings, init_settings, file_secret_settings, config_file_settings)  # type: ignore

    app: App = App()
    # postgres: dict
    storage: Storage
    map: Map
    mapStyles: dict[str, str] = {}
    tiles: Tiles
    security: Security
    geo: Geo = Geo()
    version: str
    db: DB


conf = Config(version=__version__)  # type: ignore