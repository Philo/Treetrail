import sys
from pathlib import Path
from json import dumps
import logging
from shutil import copy
from datetime import datetime
import requests
from uuid import UUID

from sqlalchemy import create_engine
from sqlmodel import select, Session, delete, create_engine as sqlmodel_create_engine
import geopandas as gpd # type: ignore
import pandas as pd

from treetrail.config import conf
from treetrail.utils import get_attachment_tree_root
from treetrail.models import Tree, Trail, TreeTrail, User, Zone
from treetrail.plantekey import Plant, fetch_browse, update_details

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

column_mapper = {
    'pic_full': 'photo',
    'Comment': 'comment',
}

base_tree_attachment_dir = get_attachment_tree_root()

def list_layers(file):
    from fiona import listlayers
    print(' '.join(f"'{x}'" for x in listlayers(file)))

def copy_image(record, base_dir):
    '''Copy the file to the proper location for attachments'''
    if pd.isna(record.photo):
        return
    file = base_dir / record.photo
    dest_dir = base_tree_attachment_dir / record.id
    dest_dir.mkdir(exist_ok=True)
    copy(file, dest_dir)

def import_trees(args) -> None:
    """ Import trees from a file containing geo data.
    The geopackage file name is expected to be strict.
    A description sould be given in the documentation"""

    contributor_id = args.username
    if contributor_id is None:
        raise Exception('A user name is required to identify the contributor')
    file_to_import = Path(args.import_trees).expanduser()

    sync_engine = create_engine(conf.db.get_pg_url())
    session = Session(sync_engine)

    # Read and format the data in the file
    gdf_trees = gpd.read_file(file_to_import, layer=args.layers or None)
    gdf_trees.rename_geometry('geom', inplace=True)
    gdf_trees.to_crs(4326, inplace=True)
    gdf_trees.rename(columns=column_mapper, inplace=True)
    # Photos: take only the file name
    # gdf_trees['photo'] = gdf_trees['photo'].str.split('/', expand=True)[1]
    gdf_trees['pic_stem'] = gdf_trees['pic_stem'].str.split('/', expand=True)[1]
    gdf_trees['contributor_id'] = contributor_id
    gdf_trees['create_date'] = pd.to_datetime(gdf_trees['Date Edited'])
    gdf_trees['id'] = gdf_trees['UUID'].str.strip('{').str.strip('}')
    gdf_trees.drop(columns='UUID', inplace=True)

    ## Determine which columns are in the database
    # ... and store the remaining in a dict datastructure to store in JSON "data" column
    gdf_existing_trees: gpd.GeoDataFrame
    gdf_existing_trees = gpd.read_postgis('select * from tree', sync_engine) # type: ignore
    unknown_columns = {col for col in gdf_trees if col not in gdf_existing_trees.columns}
    known_columns = {col for col in gdf_trees if col in gdf_existing_trees.columns}
    left_columns = {col for col in gdf_existing_trees.columns if col not in gdf_trees}
    logger.debug(f'Known columns: {known_columns}')
    logger.debug(f'Unknown left: {unknown_columns}')
    logger.debug(f'Columns left: {left_columns}')

    # Remove empty extra fields
    new_trees_data_raw = gdf_trees[list(unknown_columns)].to_dict(orient='records')
    new_trees_data = []
    for data in new_trees_data_raw:
        new_trees_data.append(
            {k: v for k, v in data.items()
            if not pd.isna(v)
            }
        )

    gdf_trees['data'] = [dumps(d) for d in new_trees_data]
    gdf_trees.drop(columns=unknown_columns, inplace=True)
    gdf_trees.reset_index(inplace=True)
    gdf_trees.drop(columns='index', inplace=True)

    # Find the trails
    gdf_trails: gpd.GeoDataFrame
    gdf_trails = gpd.read_postgis(select(Trail), sync_engine, index_col='id') # type: ignore

    # Assign trails to the new trees
    gdf_trails['zone'] = gdf_trails.to_crs(3857).buffer(150).to_crs(4326) # type: ignore
    gdf_trails.set_geometry('zone', inplace=True)

    gdf_trees[['trail', 'viewable_role_id']] = gdf_trees.sjoin(gdf_trails, how='left')[['index_right', 'viewable_role_id']]

    # Save trees to the database

    ## Remove the trees already in the DB from the datafreame to insert
    gdf_trees.set_index('id', inplace=True)
    gdf_new_trees = gdf_trees.loc[~gdf_trees.index.isin(gdf_existing_trees['id'].astype(str))].reset_index() # type:ignore
    gdf_new_trees.drop(columns='trail').to_postgis(Tree.__tablename__, sync_engine, if_exists='append')
    # Copy the images to the treetail storage dir
    gdf_new_trees.apply(copy_image, axis=1, base_dir=file_to_import.parent)
    # for file in import_image_dir.iterdir():
    #     id = file.stem.split('_')[-1]
    #     gdf_trees.photo.str.split('/', expand=True)1]
    df_tt_existing = pd.read_sql(select(TreeTrail), sync_engine)
    df_tt_existing.rename(columns={'tree_id': 'id', 'trail_id': 'trail'}, inplace=True)
    df_tt_existing['id'] = df_tt_existing['id'].astype(str)

    df_tt_new = gdf_trees['trail'].reset_index()
    df_tt_to_insert = pd.concat([df_tt_new, df_tt_existing]).drop_duplicates(keep=False) # type: ignore
    def get_tt_rel(tree):
        return TreeTrail(tree_id=tree.id, trail_id=tree.trail)
    tree_trails = df_tt_to_insert.reset_index().apply(get_tt_rel, axis=1) # type: ignore
    with Session(sync_engine) as session:
        for tt in tree_trails:
            session.add(tt)
        session.commit()
    logger.info(f'Imported on behalf of {args.username} {len(gdf_new_trees)} trees')

def import_zones(args) -> None:
    """Import a geopackage with zones.
    The format of the input file is strict.
    """
    if args.layers is None:
        print('Provide layer names from:')
        list_layers(args.import_zones)
        sys.exit(1)
    file_to_import = Path(args.import_zones).expanduser()
    fields_map = {
        'Area': 'name',
    }
    fields_ignored = [
        'fid',
        'Area Area',
        'Area type',
    ]
    sync_engine = create_engine(conf.db.get_pg_url())
    gdf_existing_zones: gpd.GeoDataFrame
    gdf_existing_zones = gpd.read_postgis('select * from zone', sync_engine) # type: ignore
    now = datetime.now()
    for layer in args.layers:
        print(layer)
        gdf = gpd.read_file(file_to_import, layer=layer)
        gdf.rename(columns=fields_map, inplace=True)
        gdf.drop(columns=fields_ignored, inplace=True, errors='ignore')
        unknown_columns = {col for col in gdf
                           if col not in gdf_existing_zones.columns}
        unknown_columns = unknown_columns - {'geometry'}
        all_data_raw = gdf[list(unknown_columns)].to_dict(orient='records')
        all_data = []
        for data in all_data_raw:
            all_data.append(
                {k: v for k, v in data.items()
                if not pd.isna(v)
                }
            )
        gdf['data'] = [dumps(d) for d in all_data]
        gdf.drop(columns=unknown_columns, inplace=True)
        gdf.reset_index(inplace=True)
        gdf.drop(columns='index', inplace=True)
        gdf['type'] = layer
        gdf['create_date'] = now
        gdf.to_crs("EPSG:4326", inplace=True)
        gdf.rename_geometry('geom', inplace=True)
        if 'name' not in gdf.columns:
            gdf['name'] = '?'
        else:
            gdf['name'].fillna('?', inplace=True)
        gdf.to_postgis(Zone.__tablename__, sync_engine,
                        if_exists='append', index=False)

def import_plantekey_trees(args, contributor_id='plantekey'):
    """Import all trees from plantekey web site
    """
    now = datetime.now()
    sync_engine = sqlmodel_create_engine(conf.db.get_pg_url())
    trail_id = int(args.import_plantekey_trees_to_trail)

    ## Harmless check that the 'plantekey' contributor exists
    with Session(sync_engine) as session:
        contributor = session.get(User, contributor_id)
    if contributor is None:
        raise UserWarning('User plantekey not found')

    ## Get the raw data from the plantekey web site
    plantekey_trees_raw = requests.get('https://plantekey.com/api.php?action=markers').json()['markers']

    ## Convert that raw data into a nice dataframe
    df_tree_plantekey = pd.DataFrame(plantekey_trees_raw).drop(columns=['0', '1', '2', '3'])
    df_tree_plantekey['MasterID'] =  df_tree_plantekey['MasterID'].astype(int)
    df_tree_plantekey.sort_values('MasterID', inplace=True)
    df_tree_plantekey.reset_index(drop=True, inplace=True)
    print(f'Found {len(df_tree_plantekey)} trees in Plantekey web site')

    ## The MasterID is probably the plant id, so just dropping it
    df_tree_plantekey.drop(columns=['MasterID'], inplace=True)

    ## Get the existing plants in the database
    df_plants = pd.read_sql(select(Plant), sync_engine)

    ## Merge those trees with the plants
    df_tree = df_tree_plantekey.merge(df_plants[['name', 'id']], left_on='name', right_on='name', how='left')
    df_tree.rename(columns={'id': 'plantekey_id'}, inplace=True)

    ## Generate a primary key (custom UUID), which is predictable: it marks the source as plantekey, and tracks the plantkey tree id
    base_fields = (0x10000001, 0x0001, 0x0001, 0x00, 0x01)
    def id_to_uuid(tree) -> UUID:
        return UUID(fields=base_fields + (tree['index'], ))
    df_tree['id'] = df_tree.reset_index().apply(lambda _: id_to_uuid(_), axis=1) # type: ignore
    #gdf_tree.drop(columns=['plantekey_tree_id'], inplace=True)
    df_tree.set_index('id', inplace=True)

    ## Detect missing plants
    missing_plants = df_tree.loc[df_tree.plantekey_id.isna()]['name'].unique()
    if len(missing_plants) > 0:
        print(f'* Warning: {len(missing_plants)} plants are missing in Treetrail, please update it!')
        print('* Missing plants:')
        for mp in missing_plants:
            print(f'    {mp}')
        df_tree = df_tree.loc[~df_tree.plantekey_id.isna()]
        print(f'* Importing only {len(df_tree)} trees.')

    ## Build the geodataframe
    gdf_tree = gpd.GeoDataFrame(
        df_tree,
        geometry=gpd.points_from_xy(df_tree["Longitude"], df_tree["Latitude"]),
        crs="EPSG:4326",
    ) # type: ignore
    gdf_tree.drop(columns=['Latitude', 'Longitude', 'name'], inplace=True)
    #gdf_tree['data'] = gdf_tree.plantekey_tree_id.apply(lambda _: dumps({'Plantekey tree id': int(_)}))
    #gdf_tree['data'] = dumps({'Source': 'Plantekey (Botanical Graden)'})
    gdf_tree.rename(columns={'geometry': 'geom'}, inplace=True)
    gdf_tree.set_geometry('geom', inplace=True)

    # Save to the database

    ## Prepare the geodataframe for saving
    gdf_tree['create_date'] = now
    gdf_tree['contributor_id'] = contributor.username
    gdf_tree['data'] = dumps({})

    ## Remove all trees with the contributor "plantekey" before adding the new ones
    with Session(sync_engine) as session:
        to_delete_trees = session.exec(select(Tree).where(Tree.contributor_id==contributor_id)).all()
        to_delete_tree_ids = [tree.id for tree in to_delete_trees]
        print(f'{len(to_delete_trees)} trees existing in the database from plantekey, deleting all of them')
        ## Also delete their relationships to that trail
        session.exec(delete(TreeTrail).where(TreeTrail.tree_id.in_(to_delete_tree_ids))) # type: ignore
        session.exec(delete(Tree).where(Tree.id.in_(to_delete_tree_ids))) # type: ignore
        session.commit()

    ## Finally insert to the database
    gdf_tree.to_postgis(Tree.__tablename__, sync_engine, if_exists='append', index=True)

    ## And add those to the trail
    with Session(sync_engine) as session:
        for tree_id in gdf_tree.index:
            session.add(TreeTrail(tree_id=tree_id, trail_id=trail_id))
        session.commit()
    print(f'Added {len(gdf_tree)} trees.')
    print('Import done.')

async def import_plantekey_plants(args):
    df = await fetch_browse()
    await update_details(df)
