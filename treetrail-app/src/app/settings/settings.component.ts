import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

import { ActionService } from '../action.service'
import { ConfigService, Config } from '../config.service'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public conf: Config
  constructor(
    public actionService: ActionService,
    public configService: ConfigService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.configService.conf.subscribe(
      conf => this.conf = conf
    )
  }
}
