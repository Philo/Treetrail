import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantBrowserComponent } from './plant-browser.component';

describe('PlantBrowserComponent', () => {
  let component: PlantBrowserComponent;
  let fixture: ComponentFixture<PlantBrowserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlantBrowserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
