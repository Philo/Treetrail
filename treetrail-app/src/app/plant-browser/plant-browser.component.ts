import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core'
import { MatTableDataSource } from '@angular/material/table'
import { MatSort, Sort } from '@angular/material/sort'

import { MessageService } from '../message.service'
import { PlantekeyService } from '../plantekey.service'
import { Plant } from '../models'

// XXX: not used anymore, needs to bu updated using dataService
@Component({
  selector: 'app-plant-browser',
  templateUrl: './plant-browser.component.html',
  styleUrls: ['./plant-browser.component.scss']
})
export class PlantBrowserComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort
  public plantsTableData: MatTableDataSource<Plant> = new MatTableDataSource()

  constructor(
    public plantekeyService: PlantekeyService,
    public messageService: MessageService,
  ) { }

  public displayedColumns = [
    'card',
    //'name',
    //'ID',
    //'english',
    //'family',
    //'hindi',
    //'spiritual',
    //'tamil',
    //'type',
    'img',
  ]

  ngOnInit(): void {
    this.plantekeyService.getAllPlants().subscribe()
    this.plantekeyService.plants.subscribe(
      plants => {
        this.plantsTableData.data = Object.values(plants)
      }
    )
  }

  ngAfterViewInit() {
    this.plantsTableData.sort = this.sort
  }

  getImgUrl(plant: Plant) {
    return '/attachment/plantekey/thumb/' + plant.img
  }
}
