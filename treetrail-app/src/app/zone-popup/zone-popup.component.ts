import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core'
import { Router } from '@angular/router'

import { Zone } from '../models'
import { flipAnimation } from '../map/animation'

@Component({
  selector: 'app-zone-popup',
  templateUrl: './zone-popup.component.html',
  styleUrl: './zone-popup.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [flipAnimation],
})
export class ZonePopupComponent {
  @Input() zone: Zone
  public isFlipped: boolean

  constructor(
    private router: Router,
    public cdr: ChangeDetectorRef,
  ) {}

  public flipCard(): void {
    this.isFlipped = !this.isFlipped;
  }
}
