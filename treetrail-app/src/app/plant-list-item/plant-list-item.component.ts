import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'

import { DataService } from '../data.service'
import { Plant } from '../models'
import { flipAnimation } from '../map/animation'

export class Detail {
  constructor(
    public key: string,
    public value: string
  ) {}
}

@Component({
  selector: 'app-plant-list-item',
  templateUrl: './plant-list-item.component.html',
  styleUrls: ['./plant-list-item.component.scss'],
  animations: [flipAnimation],
})
export class PlantListItemComponent {
  constructor(
    private router: Router,
    public dataService: DataService
  ) {}

  @Input() plant: Plant
  @Input() withImage: boolean = false
  @Input() count: number

  public isFlipped: boolean

  navigateToDetail() {
    this.router.navigate(['plant', this.plant.id])
  }

  public flipCard(): void {
    this.isFlipped = !this.isFlipped;
  }

  specimen: {[k: string]: string} = {
    '=0': 'No specimen',
    '=1': '1 specimen',
    'other': '# specimen'
  }
}