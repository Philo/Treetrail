import { Injectable } from '@angular/core'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        public router: Router,
        public route: ActivatedRoute,
    ) {
    }

    getUserDetails() {
        return localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null
    }

    setDataInLocalStorage(variableName, data) {
        localStorage.setItem(variableName, data)
    }

    getToken() {
        return localStorage.getItem('token')
    }

    clearToken() {
        localStorage.removeItem('token')
    }

    clearStorage() {
        localStorage.clear()
    }
}