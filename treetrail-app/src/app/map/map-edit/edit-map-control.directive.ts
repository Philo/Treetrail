import { AfterContentInit, Directive, Host, Input, ApplicationRef, createComponent } from '@angular/core'
import { ControlComponent, MapService, CustomControl } from '@maplibre/ngx-maplibre-gl'

import { MapEditService } from './map-edit.service'
import { MapEditComponent } from './map-edit.component'

export class EditControl extends CustomControl {
  private _container: HTMLElement
  constructor(
    container: HTMLElement,
    public appRef: ApplicationRef,
  ) {
    super(container)
    container.classList.add('maplibregl-ctrl-group')
    this._container = container
  }

  onAdd() {
    const componentRef = createComponent(MapEditComponent, {
      hostElement: this._container,
      environmentInjector: this.appRef.injector,
    })
    this._container.title = "Add tree"
    this.appRef.attachView(componentRef.hostView)
    return this._container
  }
}

@Directive({
  selector: '[treeTrailMapEdit]',
})
export class TreeTrailMapEditControlDirective implements AfterContentInit {
  @Input() container?: HTMLElement
  constructor(
    private mapService: MapService,
    public mapEditComponentService: MapEditService,
    public appRef: ApplicationRef,
    @Host() private controlComponent: ControlComponent<EditControl>
  ) {}

  ngAfterContentInit() {
    this.mapService.mapCreated$.subscribe(() => {
      if (this.controlComponent.control) {
        throw new Error('Another control is already set for this control')
      }

      this.controlComponent.control = new EditControl(
        this.controlComponent.content.nativeElement,  //this.container,
        this.appRef,
      )

      this.mapService.addControl(
        this.controlComponent.control,
        this.controlComponent.position
      )
    })
  }
}