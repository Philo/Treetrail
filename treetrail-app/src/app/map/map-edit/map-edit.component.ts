import { Component, ChangeDetectorRef,
  ChangeDetectionStrategy, ApplicationRef, OnInit } from '@angular/core'

import { MessageService } from '../../message.service'
import { MapEditService } from './map-edit.service'

@Component({
  selector: 'app-map-edit',
  templateUrl: './map-edit.component.html',
  styleUrls: ['./map-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapEditComponent implements OnInit {
  constructor(
    public mapEditService: MapEditService,
    public cdr: ChangeDetectorRef,
    public appRef: ApplicationRef,
    public messageService: MessageService,
  ) {}

  ngOnInit(): void {
    this.mapEditService.cancelEditMap$.subscribe(
      () => {
        this.mapEditService.active = false
        this.cdr.markForCheck()
      }
    )
  }

  toggle() {
    this.mapEditService.toggle()
    if (this.mapEditService.active) {
      this.messageService.message.next(
        'Add trees by clicking on their location on the map'
      )
    }
    this.cdr.markForCheck()
    this.appRef.tick()
  }
}
