import { Component, ElementRef, OnInit,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'

import { Subscription } from 'rxjs'

import { FeatureFinderService } from '../feature-finder.service'


@Component({
  selector: 'app-direction',
  templateUrl: './direction.component.html',
  styleUrls: ['./direction.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TreetrailDirectionComponent implements OnInit {
  direction: number
  clickSubscription: Subscription
  distance: number
  orientationClass: string = 'unknown'

  constructor(
    public elementRef: ElementRef,
    public featureFinderService: FeatureFinderService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit() {
    this.featureFinderService.direction$.subscribe(
      dir => {
        this.direction = dir
        this.cdr.markForCheck()
      }
    )

    this.featureFinderService.distance$.subscribe(
      dist => {
        this.distance = Math.round(dist)
        this.cdr.markForCheck()
      }
    )

    this.featureFinderService.direction$.subscribe(
      _ => this.cdr.markForCheck()
    )

    this.featureFinderService.orientation$.subscribe(
      orientation => {
        if (typeof(orientation) != 'undefined') {
          this.orientationClass = 'absolute'
        }
        else {
          this.orientationClass = 'unknown'
        }
        this.cdr.markForCheck()
      }
    )
  }
}