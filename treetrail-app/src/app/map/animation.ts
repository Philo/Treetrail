import { trigger, transition, animate, style, keyframes, state } from '@angular/animations'

export const flipAnimation = trigger('flip', [
    state('front', style({
        transform: 'rotateY(0deg)'
    })),
    state('back', style({
        transform: 'rotateY(180deg)'
    })),
    transition('front => back', [
        animate('0.4s 0s ease-out',
            keyframes([
                style({
                    transform: 'perspective(400px) rotateY(180deg)',
                    offset: 1
                })
            ])
        )
    ]),
    transition('back => front', [
        animate('0.4s 0s ease-in',
            keyframes([
                style({
                    transform: 'perspective(400px) rotateY(0deg)',
                    offset: 1
                })
            ])
        )
    ])
])