import { Component, OnInit,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'
import { Observable, BehaviorSubject } from 'rxjs'
import { map } from 'rxjs/operators'

import { ActionService } from '../action.service'
import { FeatureFinderService } from '../feature-finder.service'

@Component({
  selector: 'app-indicator',
  templateUrl: './indicator.component.html',
  styleUrls: ['./indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndicatorComponent implements OnInit {

  constructor(
    public actionService: ActionService,
    public featureFinderService: FeatureFinderService,
    private cdr: ChangeDetectorRef,
  ) { }

  distance: number

  ngOnInit(): void {
    this.featureFinderService.distance$.subscribe(
      dist => {
        this.distance = Math.round(dist)
        this.cdr.markForCheck()
      }
    )

    this.featureFinderService.direction$.subscribe(
      _ => this.cdr.markForCheck()
    )

    this.featureFinderService.orientation$.subscribe(
      _ => this.cdr.markForCheck()
    )
  }
}
