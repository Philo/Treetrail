import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'

import { Tree } from '../models'
import { DataService } from '../data.service'

@Component({
  selector: 'app-tree-detail',
  templateUrl: './tree-detail.component.html',
  styleUrls: ['./tree-detail.component.scss']
})
export class TreeDetailComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public dataService: DataService
  ) { }

  tree: Tree

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.dataService.all.pipe(
          map(
            all => {
              if (all.trees) {
                this.tree = all.trees[params.get('pekid')]
                console.log(this.tree)
              }
            }
          )
        )
      )
    )
    .subscribe()
  }

  navigateToDetail() {
    // this.router.navigate(['plant', this.tree.plant.id])
    this.router.navigate(['plant', this.tree.plant.id])
  }

  navigateToTrail(trail) {
    this.router.navigate(['trail', trail.id])
  }
}