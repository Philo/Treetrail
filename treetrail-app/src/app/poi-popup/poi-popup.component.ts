import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, ViewChild } from '@angular/core'

import { Poi } from '../models'
import { ActionService } from '../action.service'
import { flipAnimation } from '../map/animation'

@Component({
  selector: 'app-poi-popup',
  templateUrl: './poi-popup.component.html',
  styleUrls: ['./poi-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [flipAnimation]})
export class PoiPopupComponent {
  @Input() poi: Poi
  @ViewChild("fileDropRef", { static: false }) fileDropEl: ElementRef
  public isFlipped: boolean
  constructor(
    public cdr: ChangeDetectorRef,
    public actionService: ActionService
  ) {}

  public flipCard(): void {
    this.isFlipped = !this.isFlipped;
  }

  onFileDropped(fileList: FileList) {
    const formData = new FormData()
    formData.set("file", fileList.item(0))
    this.actionService.upload('poi', 'photo', this.poi.id.toString(), formData).subscribe(
      res => {
        this.poi.photo = res['filename']
        this.cdr.markForCheck()
      }
    )
  }

  fileBrowseHandler(evt) {
    console.log(evt)
  }
}
