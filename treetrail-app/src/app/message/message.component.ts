import { Component, AfterViewInit,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'

import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar'

import { MessageService } from '../message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageComponent implements AfterViewInit {
  constructor(
    public messageService: MessageService,
    private cdr: ChangeDetectorRef,
    private _snackBar: MatSnackBar
  ) {}

  durationInSeconds: number = 2.5

  ngAfterViewInit(): void {
    this.messageService.message$.subscribe(
      message => {
        if (!!message) {
          this._snackBar.open(message, 'OK', {
            duration: this.durationInSeconds * 1000,
          })
        }
        this.cdr.markForCheck()
      }
    )
  }
}
