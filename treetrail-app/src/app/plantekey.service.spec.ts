import { TestBed } from '@angular/core/testing';

import { PlantekeyService } from './plantekey.service';

describe('PlantekeyService', () => {
  let service: PlantekeyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlantekeyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
