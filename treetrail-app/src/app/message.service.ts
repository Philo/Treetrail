import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public message = new BehaviorSubject<string>(undefined)
  public message$ = this.message.asObservable()

  public spinner = new BehaviorSubject<boolean>(false)
  public spinner$ = this.spinner.asObservable()

  constructor() { }
}
