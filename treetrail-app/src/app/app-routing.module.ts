import { NgModule, inject } from '@angular/core'
import {
  ActivatedRouteSnapshot, ResolveFn, RouterModule,
  RouterStateSnapshot, Routes
} from '@angular/router'

import { HomeComponent } from './home/home.component'
import { IntroComponent } from './intro/intro.component'
import { MapViewComponent } from './map-view/map-view.component'
import { PlantBrowserComponent } from './plant-browser/plant-browser.component'
import { PlantListComponent } from './plant-list/plant-list.component'
import { PlantDetailComponent } from './plant-detail/plant-detail.component'
import { TreeDetailComponent } from './tree-detail/tree-detail.component'
import { SettingsComponent } from './settings/settings.component'
import { AdminComponent } from './admin/admin.component'
import { AboutComponent } from './about/about.component'
import { TrailListComponent } from './trail-list/trail-list.component'
import { TrailDetailComponent } from './trail-detail/trail-detail.component'
import { LoginComponent } from './login/login.component'
import { ProfileComponent } from './profile/profile.component'
import { AuthGuardService } from './services/auth-guard.service'


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'settings',
    component: SettingsComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'trail',
    component: TrailListComponent,
  },
  {
    path: 'trail/:id',
    component: TrailDetailComponent,
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'map',
    component: MapViewComponent,
  },
  {
    path: 'intro',
    component: IntroComponent,
  },
  {
    path: 'plant-table',
    component: PlantBrowserComponent,
  },
  {
    path: 'tree/:pekid',
    component: TreeDetailComponent,
  },
  {
    path: 'plant/:pekid',
    component: PlantDetailComponent,
  },
  {
    path: 'plant',
    component: PlantListComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
