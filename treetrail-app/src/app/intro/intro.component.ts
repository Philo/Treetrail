import { Component, OnInit, Input,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

import { ActionService } from '../action.service'

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IntroComponent implements OnInit {
  @Input() showSettings: boolean = true

  constructor(
    public actionService: ActionService,
    private cdr: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
  }

}
