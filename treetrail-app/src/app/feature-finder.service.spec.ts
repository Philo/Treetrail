import { TestBed } from '@angular/core/testing';

import { FeatureFinderService } from './feature-finder.service';

describe('FeatureFinderService', () => {
  let service: FeatureFinderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeatureFinderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
