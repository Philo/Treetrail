import { Component, OnInit,
  ChangeDetectorRef, ChangeDetectionStrategy, HostListener } from '@angular/core';

import { ActionService } from '../action.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit {
  constructor(
    private cdr: ChangeDetectorRef,
    public actionService: ActionService,
  ) { }

  ngOnInit(): void {
  }

  public promptEvent

  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(e) {
    console.log('Ready to install', e)
    e.preventDefault()
    this.promptEvent = e
    this.cdr.markForCheck()
  }

  public installPWA() {
    this.promptEvent.prompt()
  }

  public shouldInstall(): boolean {
    return !this.isRunningStandalone() && this.promptEvent
  }

  public isRunningStandalone(): boolean {
    return (window.matchMedia('(display-mode: standalone)').matches)
  }
}
