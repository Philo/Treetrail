import { Component, OnInit,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'
import { DataService } from './data.service'

import { ActionService } from './action.service'
import { AppUpdateService } from './app-update.service'
import { ConfigService, settingsDbName } from './config.service'
import { combineLatest } from 'rxjs'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  constructor(
    public dataService: DataService,
    public configService: ConfigService,
    public actionService: ActionService,
    public appUpdateService: AppUpdateService,
    public cdr: ChangeDetectorRef,
  ) {}
  title = 'treetrail'

  ngOnInit(): void {
    this.actionService.fetchData().subscribe()
  }
}
