import { Component, OnInit,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'
import { DataService } from '../data.service'

import { MessageService } from '../message.service'
import { Plant } from '../models'

@Component({
  selector: 'app-plant-list',
  templateUrl: './plant-list.component.html',
  styleUrls: ['./plant-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlantListComponent implements OnInit {
  showAll: boolean = false
  searchText: string
  constructor(
    public messageService: MessageService,
    private cdr: ChangeDetectorRef,
    public dataService: DataService
  ) {}

  filter() {
    this.cdr.markForCheck()
  }

  filterView(plant: Plant) {
    if (!!this.searchText) {
      return plant.isMatch(this.searchText)
    }
    else {
      return this.showAll || this.dataService.plant_in_some_trail(plant.id)
    }
  }

  ngOnInit(): void {
    this.dataService.all.subscribe()
  }
}
