import { Component, OnInit,
  ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'

import { DataService } from '../data.service'
import { ActionService } from '../action.service'


@Component({
  selector: 'app-trail-list',
  templateUrl: './trail-list.component.html',
  styleUrls: ['./trail-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrailListComponent implements OnInit {
  constructor(
    public actionService: ActionService,
    public dataService: DataService,
    public cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
  }
}
