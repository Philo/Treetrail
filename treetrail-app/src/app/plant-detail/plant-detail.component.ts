import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'

import { Plant } from '../models'
import { DataService } from '../data.service'

@Component({
  selector: 'app-plant-detail',
  templateUrl: './plant-detail.component.html',
  styleUrls: ['./plant-detail.component.scss']
})
export class PlantDetailComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public dataService: DataService
  ) { }

  include = new Set([
    'description', 'habit', 'landscape', 'uses', 'planting', 'propagation',
    'type', 'element', 'hindi', 'tamil', 'woody', 'latex',
    'leaf_style', 'leaf_type', 'leaf_arrangement', 'leaf_aroma', 'leaf_length', 'leaf_width',
    'flower_color', 'flower_aroma', 'flower_size',
    'fruit_color', 'fruit_size', 'fruit_type',
    'thorny'])

  plant: Plant

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.dataService.all.pipe(
          map(
            all => this.plant = all.plants[params.get('pekid')]
          )
        )
      )
    )
    .subscribe()
  }

  get imgUrl() {
    //return 'https://plantekey.com/slir/?p=1&w=300&i=/admin/images/plants/' + this.plant.img
    return '/attachment/plantekey/thumb/' + this.plant.img
  }

  /*
  get details(): any[] {
    return Object.entries(this.plant).filter(e => this.include.has(e[0]))
  }
  */
}
