import { defineConfig } from '@hey-api/openapi-ts';

export default defineConfig({
  input: 'http://127.0.0.1:5002/v1/openapi.json',
  output: 'src/app/openapi',
  client: 'angular',
});
